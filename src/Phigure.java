public abstract class Phigure {
    private double Height, Width;
    private String Name;

    public Phigure(String Name,double Height, double Width){
        this.Name = Name;
        this.Height = Height;
        this.Width = Width;
    }

    public String getName() {
        return Name;
    }

    public double getHeight() {
        return Height;
    }

    public double getWidth() {
        return Width;
    }

    public abstract String toString();
}

class Rectangle extends Phigure{
    private double S, P;
    public Rectangle(String Name, double Height, double Width, double S, double P){
        super(Name, Height, Width);
        this.S = S;
        this.P = P;
    }

    public String toString(){
        P = 2 * (super.getHeight() * super.getWidth());
        S = super.getHeight() * super.getWidth();
        return String.format("Название фигуры: %s\nВысоты и ширина: (%f; %f)\n P = %f; S = %f\n^v^v^\n", super.getName(), super.getHeight(), super.getWidth(), P, S);
    }
}

class Circle extends Phigure{
    private double R, S, P;

    public Circle(String Name, double Height, double Width, double R, double P, double S) {
        super(Name, Height, Width);
        this.R = R;
        this.S = S;
        this.P = P;
    }

    public String toString(){
        P = 2*3.14*R;
        S = 3.14*(R*R);
        return String.format("Название фигуры: %s\nРадиус: (%f)\n P = %f; S = %f\n---\n", super.getName(), R, P, S);
    }
}

class Triangle extends Phigure {
    private double C, S, P;

    public Triangle(String Name, double Height, double Width, double C, double P, double S) {
        super(Name, Height, Width);
        this.C = C;
        this.S = S;
        this.P = P;
    }

    public String toString(){
        P = super.getHeight() + super.getWidth() + C;
        S = 1/2 * (super.getHeight() * getWidth());
        return String.format("Название фигуры: %s\nСтороны: (%f;%f;%f;)\n P = %f; S = %f\n---\n", super.getName(), super.getHeight(), super.getWidth(), C, P, S);
    }
}
