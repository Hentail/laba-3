import java.util.*;
public class Main {
    public static void main(String[] args) {
        List<Phigure> figureList = new ArrayList<>();
        figureList.add(new Rectangle("Прямоугольник",3.6,2, 0, 0));
        figureList.add(new Circle("Круг",0,0, 5, 0, 0));
        figureList.add(new Triangle("Треугольник",4, 4.2, 4, 0, 0));
        for (Phigure a:figureList) {
            System.out.printf(a.toString());
        }
    }
}